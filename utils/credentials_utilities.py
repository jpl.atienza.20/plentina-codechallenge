import os

def get_s3_bucket():
    """Returns S3 Bucket Name"""
    return os.getenv('s3_bucket_name')

def get_db_credentials():
    """Returns Database Credentials"""
    return {"username":os.getenv('db_user'),
            "password":os.getenv('db_pass'),
            "host":os.getenv('db_host'),
            "db_name":os.getenv('db_name'),
            "port":os.getenv('db_port')
    }

def get_s3_credentials():
    return(os.getenv('s3_access_id'),os.getenv('s3_secret_key'))