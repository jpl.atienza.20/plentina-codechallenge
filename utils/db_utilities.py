from sqlalchemy import create_engine

def get_redshift_engine(db_credentials):
    """Create a sqlalchemy engine object to access the db
            db_credentials {
                "username": username of the user accessing the db
                "password": password of the  user accessing the db
                "host": Redshift endpoint of the db
                "port": port number
                "db_name": name of db to access
            }"""
    engine = create_engine(f"postgresql://{db_credentials['username']}:{db_credentials['password']}@{db_credentials['host']}:{db_credentials['port']}/{db_credentials['db_name']}")
    return engine