import boto3
from botocore.exceptions import NoCredentialsError

def upload_to_aws(local_file,credentials, bucket, s3_file):
    """Code to upload a local file to s3
            local_file: filepath to the file to be uploaded
            credentials: tuple of the access id and secret key to upload the data to the s3 bucket
            s3_file: filepath where the file will be stored in the s3 bucket"""
    s3 = boto3.client('s3', aws_access_key_id=credentials[0],
                      aws_secret_access_key=credentials[1])

    try:
        s3.upload_file(local_file, bucket, s3_file)
        print("Upload Successful",end='\r')
        return True
    except FileNotFoundError:
        print("The file was not found")
        return False
    except NoCredentialsError:
        print("Credentials not available")
        return False
