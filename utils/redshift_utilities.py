
def insert_query_factory(table_name,s3path,credentials):
    """Truncates the original table and inserts the data for the Redshift db
            table_name: name of the table to be inserted. Must include schema name
            s3_path: s3 bucket path of the parquet file to be uploaded
            credentials: tuple of the access id and secret key to upload the data to the s3 bucket"""
    query = """

    TRUNCATE TABLE {table_name};

    COPY {table_name}
    FROM '{s3path}'
    ACCESS_KEY_ID '{access_id}'
    SECRET_ACCESS_KEY '{secret_key}'
    FORMAT AS PARQUET;
    """.format(table_name=table_name,
               s3path=s3path,
               access_id=credentials[0],
               secret_key=credentials[1])
    return query