from sqlalchemy import text

def find_match_id_with_hero(engine,hero_id):
    with engine.begin() as connection:
        results = connection.execute(find_match_id_containing_hero_query(),{'hero_id':hero_id}).fetchall()
    return [x[0] for x in results]

def find_match_id_containing_hero_query():
    """Query to find match id's where a certain hero is present"""
    query =text("""SELECT
        mt.match_id
    FROM
        dota.vw_match_teams mt
    GROUP BY
        mt.match_id
    HAVING
        COUNT( CASE hero_id_01 WHEN :hero_id THEN 1 END) > 0
        OR
            COUNT( CASE hero_id_02 WHEN :hero_id THEN 1 END) > 0
        OR
            COUNT( CASE hero_id_03 WHEN :hero_id THEN 1 END) > 0
        OR
            COUNT( CASE hero_id_04 WHEN :hero_id THEN 1 END) > 0
        OR
            COUNT( CASE hero_id_05 WHEN :hero_id THEN 1 END) > 0
    ORDER BY mt.match_id
    """)
    return query