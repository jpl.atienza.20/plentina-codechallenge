
from sqlalchemy import select, func
from sqlalchemy.sql.expression import column

def count_column_entries(engine,table_obj,column_name):
    with engine.begin() as connection:
        count = connection.execute(select(func.count(table_obj.columns[column_name]))).scalars().one()
    return count