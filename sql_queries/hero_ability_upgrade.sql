CREATE OR REPLACE VIEW dota.vw_hero_ability_upgrade AS
SELECT
	au.match_id,
	me.hero_id,
	me.region,
	me.division,
	au.level ,
	au.ability
FROM
	dota.ability_upgrades au
INNER JOIN dota.vw_valid_player_match_entries me
	ON
	me.match_id = au.match_id
	AND
	   me.player_slot = au.player_slot
