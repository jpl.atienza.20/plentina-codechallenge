CREATE OR REPLACE VIEW dota.vw_global_hero_ability_winrates AS
SELECT
	hn.localized_name,
	ai.ability_name,
	au.level,
	COUNT(DISTINCT au.match_id) AS num_matches,
	COUNT(CASE au.match_id WHEN me.win_match THEN 1 ELSE NULL END) AS num_wins,
	(1.0* num_wins/NULLIF(num_matches,0))*100 AS win_rate 
FROM
	dota.vw_hero_ability_upgrade au
LEFT JOIN dota.vw_valid_player_match_entries me
	ON
	au.match_id = me.match_id
	AND
	   au.hero_id = me.hero_id
LEFT JOIN dota.ability_ids ai
	ON
	ai.ability_id = au.ability
LEFT JOIN dota.hero_names hn
	ON
	hn.hero_id = au.hero_id
GROUP BY
	hn.localized_name ,
	ai.ability_name,
	au.LEVEL
