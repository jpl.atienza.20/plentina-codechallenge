CREATE OR REPLACE VIEW dota.vw_global_hero_item_winrates AS
SELECT
	hn.localized_name,
	i.item_name,
	COUNT(DISTINCT match_id) AS num_matches,
	COUNT(CASE fi.match_id WHEN fi.win_match THEN 1 ELSE NULL END) AS num_wins,
	100.0 * num_wins / NULLIF(num_matches, 0) AS win_rate
FROM
	dota.vw_player_final_items fi
LEFT JOIN dota.hero_names hn
	ON
	fi.hero_id = hn.hero_id
LEFT JOIN dota.item_ids i
	ON
	i.item_id = fi.item
GROUP BY
	hn.localized_name,
	i.item_name