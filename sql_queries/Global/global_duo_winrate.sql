CREATE OR REPLACE VIEW dota.vw_global_duo_winrate AS
SELECT
	hn1.localized_name AS hero_1,
	hn2.localized_name AS hero_2,
	COUNT(CASE s.match_id WHEN s.win THEN 1 ELSE NULL END) AS num_wins,
	COUNT(s.match_id) AS num_matches,
	100.0*num_wins/NULLIF(num_matches,0) AS win_rate,
--	Definitely a Hack for searching hero names due to PowerBI limitations, 
	regexp_replace('"' ||hn1.localized_name ||'"' ||hn2.localized_name||'"','\\s','') AS hero_concat
	 
FROM
	dota.synergies s
LEFT JOIN dota.vw_valid_player_match_entries me
	ON
	me.match_id = s.match_id
LEFT JOIN dota.hero_names hn1
	ON
	hn1.hero_id = s.hero_1
LEFT JOIN dota.hero_names hn2
	ON
	hn2.hero_id = s.hero_2
GROUP BY
	hn1.localized_name,
	hn2.localized_name