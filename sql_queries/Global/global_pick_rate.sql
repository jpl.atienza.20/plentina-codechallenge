CREATE OR REPLACE VIEW dota.vw_global_hero_pick_rate AS
SELECT
	hn.localized_name,
	a.num_hero_matches,
	(
	SELECT
		COUNT(DISTINCT me.match_id) AS total_matches
	FROM
		dota.vw_valid_player_match_entries me ) AS total_matches,
	100.0*a.num_hero_matches / NULLIF(total_matches,0) AS pick_rate
FROM 
	(
	SELECT
		hero_id,
		COUNT(DISTINCT match_id) AS num_hero_matches
	FROM
		dota.vw_valid_player_match_entries
	GROUP BY
		hero_id) a
LEFT JOIN dota.hero_names hn
	ON hn.hero_id = a.hero_id