CREATE OR REPLACE VIEW dota.vw_global_hero_metrics AS
SELECT hn.localized_name AS hero,
	   COUNT(CASE a.match_id WHEN a.win_match THEN 1 ELSE NULL END) AS num_wins,
	   COUNT(DISTINCT a.match_id) AS num_matches,
	   100.0*num_wins/NULLIF(num_matches,0) AS win_rate,
	   AVG(pr.pick_rate) AS pick_rate,
	   AVG(NULLIF(a.duration/60,0)) AS avg_match_duration,
	   AVG(CASE a.duration WHEN a.win_match THEN a.duration ELSE NULL END)/60 AS avg_duration_when_win,
	   AVG(NULLIF(a.gold_per_min,0)) AS gpm,
	   AVG(NULLIF(a.xp_per_min,0)) AS xpm,
	   AVG(NULLIF(a.level,0)) AS avg_level,
	   AVG(NULLIF(1.0*a.kills,0)) AS avg_kills,
	   AVG(NULLIF(1.0*a.assists,0)) AS avg_assists,
	   AVG(NULLIF(1.0*a.deaths,0)) AS avg_deaths,
	   NULLIF((avg_kills+avg_assists)/avg_deaths,0) AS avg_kda,
	   AVG(NULLIF(a.denies+a.last_hits,0)) AS avg_creep_score,
	   AVG(NULLIF(a.first_blood_time,0)) AS avg_first_blood_time

FROM
	dota.vw_valid_player_match_entries a
LEFT JOIN dota.hero_names hn
	ON a.hero_id = hn.hero_id
LEFT JOIN dota.vw_hero_pick_rate pr
	ON hn.localized_name = pr.hero AND
	   a.region = pr.region AND
	   a.division = pr.division
GROUP BY hn.localized_name