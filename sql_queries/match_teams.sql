CREATE OR REPLACE VIEW dota.vw_match_teams AS
SELECT a.*,
       m.radiant_win,
       CASE WHEN m.radiant_win AND a.radiant THEN TRUE
            WHEN NOT m.radiant_win AND NOT a.radiant THEN TRUE
            ELSE FALSE 
            END AS win_match
FROM
	(SELECT
		match_id,
		max(CASE WHEN seqnum = 1 THEN account_id END) AS account_id_01,
		max(CASE WHEN seqnum = 2 THEN account_id END) AS account_id_02,
		max(CASE WHEN seqnum = 3 THEN account_id END) AS account_id_03,
		max(CASE WHEN seqnum = 4 THEN account_id END) AS account_id_04,
		max(CASE WHEN seqnum = 5 THEN account_id END) AS account_id_05,
		max(CASE WHEN seqnum = 1 THEN hero_id END) AS hero_id_01,
		max(CASE WHEN seqnum = 2 THEN hero_id END) AS hero_id_02,
		max(CASE WHEN seqnum = 3 THEN hero_id END) AS hero_id_03,
		max(CASE WHEN seqnum = 4 THEN hero_id END) AS hero_id_04,
		max(CASE WHEN seqnum = 5 THEN hero_id END) AS hero_id_05,
		TRUE AS radiant
	FROM
		(
		SELECT
			t.*,
			ROW_NUMBER() OVER (PARTITION BY match_id
		ORDER BY
			player_slot ASC) AS seqnum
		FROM
			dota.players t
		WHERE
			player_slot <= 10
	     ) t
	GROUP BY
		match_id
	
	UNION
	
	SELECT
		match_id,
		max(CASE WHEN seqnum = 1 THEN account_id END) AS account_id_01,
		max(CASE WHEN seqnum = 2 THEN account_id END) AS account_id_02,
		max(CASE WHEN seqnum = 3 THEN account_id END) AS account_id_03,
		max(CASE WHEN seqnum = 4 THEN account_id END) AS account_id_04,
		max(CASE WHEN seqnum = 5 THEN account_id END) AS account_id_05,
		max(CASE WHEN seqnum = 1 THEN hero_id END) AS hero_id_01,
		max(CASE WHEN seqnum = 2 THEN hero_id END) AS hero_id_02,
		max(CASE WHEN seqnum = 3 THEN hero_id END) AS hero_id_03,
		max(CASE WHEN seqnum = 4 THEN hero_id END) AS hero_id_04,
		max(CASE WHEN seqnum = 5 THEN hero_id END) AS hero_id_05,
		FALSE AS radiant
	FROM
		(
		SELECT
			t.*,
			ROW_NUMBER() OVER (PARTITION BY match_id
		ORDER BY
			player_slot ASC) AS seqnum
		FROM
			dota.players t
		WHERE
			player_slot >= 10
	     ) t
	GROUP BY
		match_id) a
LEFT JOIN dota.match m
	ON a.match_id = m.match_id
WHERE m.match_id IN (SELECT DISTINCT me.match_id FROM dota.vw_valid_player_match_entries me)