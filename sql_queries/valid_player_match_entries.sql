CREATE OR REPLACE VIEW dota.vw_valid_player_match_entries AS
SELECT p.*,
	   CASE WHEN rd.division IS NULL THEN 0 ELSE rd.division END AS division,
	   pr.trueskill_mu,
	   pr.trueskill_sigma,
	   m.radiant_win,
	   m.duration,
	   m.first_blood_time,
	   cr.region,
	   CASE
	   		WHEN p.player_slot < 10 THEN TRUE
	   		ELSE FALSE
	   	END AS radiant,
	   CASE
	   		WHEN p.player_slot < 10 AND m.radiant_win THEN TRUE
	   		WHEN p.player_slot > 10 AND NOT m.radiant_win THEN TRUE
	   		ELSE FALSE
	   	END AS win_match
FROM dota.players p
LEFT JOIN dota."match" m
	ON p.match_id = m.match_id
LEFT JOIN dota.player_ratings pr
	ON p.account_id = pr.account_id
LEFT JOIN dota.cluster_regions cr
	ON m.cluster = cr.cluster
LEFT JOIN dota.rank_divisions rd
	ON pr.trueskill_mu >= rd.lower_bound AND
	   pr.trueskill_mu < rd.upper_bound
WHERE m.game_mode = 22 AND
      m.duration >= 300 AND
      m.match_id NOT IN (SELECT DISTINCT p.match_id 
						 FROM dota.players p 
						 WHERE p.leaver_status !=0 )	