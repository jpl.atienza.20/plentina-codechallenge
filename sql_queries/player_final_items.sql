CREATE OR REPLACE VIEW dota.vw_player_final_items AS
SELECT *
FROM
(SELECT
	match_id,
	region,
	division,
	hero_id,
	item_0 AS item,
	win_match
FROM
	dota.vw_valid_player_match_entries me
UNION
SELECT
	match_id,
	region,
	division,
	hero_id,
	item_1 AS item,
	win_match
FROM
	dota.vw_valid_player_match_entries me
UNION
SELECT
	match_id,
	region,
	division,
	hero_id,
	item_2 AS item,
	win_match
FROM
	dota.vw_valid_player_match_entries me
UNION
SELECT
	match_id,
	region,
	division,
	hero_id,
	item_3 AS item,
	win_match
FROM
	dota.vw_valid_player_match_entries me
	UNION
SELECT
	match_id,
	region,
	division,
	hero_id,
	item_4 AS item,
	win_match
FROM
	dota.vw_valid_player_match_entries me
UNION
SELECT
	match_id,
	region,
	division,
	hero_id,
	item_5 AS item,
	win_match
FROM
	dota.vw_valid_player_match_entries me)
WHERE item != 0