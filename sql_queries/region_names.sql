CREATE OR REPLACE VIEW dota.vw_region_names AS
SELECT DISTINCT region
FROM dota.cluster_regions cr