CREATE OR REPLACE VIEW dota.vw_hero_pick_rate AS
SELECT
	a.hero,
	a.region,
	a.division,
	100.0 * a.num_matches / b.reg_div_match_num AS pick_rate
FROM
	(
	SELECT
		hn.localized_name AS hero,
		   me.region,
		   CASE WHEN me.division IS NULL THEN 0 ELSE me.division END AS division,
		   COUNT(DISTINCT me.match_id) AS num_matches
	FROM
		dota.vw_valid_player_match_entries me
	LEFT JOIN dota.hero_names hn
		ON
		me.hero_id = hn.hero_id
	GROUP BY
		hn.localized_name,
		me.region,
		me.division) a

LEFT JOIN (SELECT region,
  				  CASE WHEN division IS NULL THEN 0 ELSE division END AS division,
  				  COUNT(DISTINCT match_id) AS reg_div_match_num
		   FROM dota.vw_valid_player_match_entries me
		   GROUP BY me.region, me.division) b
	ON a.region = b.region AND
	   a.division = b.division